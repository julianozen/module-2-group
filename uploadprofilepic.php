<?php
session_start();
 
// Get the filename and make sure it is valid
$error=""; //for debugging
$filename = basename($_FILES['uploadedfile']['name']);
if( !preg_match('/^[\w_\.\-]+$/', $filename) ){
	$error="Invalid filename";
}
$x = explode('.',$filename);
$extension = $x[1];
if ($extension!="jpg"&&$extension!="png"&&$extension!="gif"&&$extension!="bmp") {
	$error="Invalid file type";
} 
 
// Get the username and make sure it is valid
$username = $_SESSION['user'];
if( !preg_match('/^[\w_\-]+$/', $username) ){
	$error="Invalid username:".$username;
}

if ($error=="") {
	$full_path = sprintf("/srv/uploads/profile_pic/%s.%s",$username,$extension);
 
	if( move_uploaded_file($_FILES['uploadedfile']['tmp_name'], $full_path) ){
		header("Location: profile.php");
		exit;
	}else{
		$error="Cannot move uploaded file:".$full_path;
		header("Location: profile.php");
		exit;
	}
} else {
	header("Location: profile.php");
	exit;
}
 
?>