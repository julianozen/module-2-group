<?
	$u = $_POST['user'];
	if (!isset($u)||!ctype_alpha($u)) {
		header("Location: login.php?err=1");
	} else {	
		$validuser=false;
		$h = fopen("/srv/users.txt","r");
		while( !feof($h) ){
			$s = fgets($h);
			if (trim($s)==$u) $validuser=true;
		}
 		fclose($h);
		if (!$validuser) {
			header("Location: login.php?err=1");
		} else {
			session_start();
			$_SESSION['user']=$u;
			$_SESSION['token'] = substr(md5(rand()), 0, 10);
			header("Location: profile.php");
		}
	}

?>
